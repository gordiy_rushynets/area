//
//  main.cpp
//  area
//
//  Created by Gordiy Rushynets on 2/25/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Area
{
protected:
    double len;
    double width;
public:
    virtual double area()
    {
        return len*width;
    }
};

class Rectangle : public Area
{
public:
    friend istream &operator>>(istream &in, Rectangle &rec)
    {
        return in >> rec.len >> rec.width;
    }
    
    friend ostream &operator<<(ostream &out, const Rectangle &rec)
    {
        return out << ' ' << rec.len << ' ' << rec.width;
    }

    void set_len(double length)
    {
        len = length;
    }
    
    void set_width(double wid)
    {
        width = wid;
    }
    
    double get_rectangle_len()
    {
        return len;
    }
    
    double get_rectangle_width()
    {
        return width;
    }
};

class Round : public Area
{
private:
    const double pi = 3.14;
    double circle_round;
public:
    void set_circle_round(double round)
    {
        circle_round = round;
    }
    
    double area(){
        return pi*(circle_round*circle_round);
    }
    
    double area(double round){
        circle_round = round;
        return pi*(circle_round*circle_round);
    }
};

class AngledTriangle : public Area
{
public:
    void set_len(double length)
    {
        len = length;
    }
    
    void set_width(double triangle_width)
    {
        width = triangle_width;
    }
    
    double area()
    {
        return (Area::area())/2;
    }
};

class Trapezium: public Area
{
private:
    double high;
public:
    void set_a(double length)
    {
        len = length;
    }
    
    void set_b(double triangle_width)
    {
        width = triangle_width;
    }
    
    void set_high(double trapezium_high)
    {
        high = trapezium_high;
    }
    
    double area()
    {
        return ((len + width)/2)*high;
    }
};

double rectangle_area(Rectangle rectangle, int count_rectangles = 0)
{
    double length;
    double width;
    
    ifstream fin_rectangle("/Users/gordiy/cpp/area/area/rectangle.txt");
    
    fin_rectangle >> length;
    fin_rectangle >> width;
    
    rectangle.set_len(length);
    rectangle.set_width(width);
    
    return rectangle.area();
}

double round_area(Round round)
{
    double circle_round;
    ifstream fin_round("/Users/gordiy/cpp/area/area/round.txt");
    
    fin_round >> circle_round;
    
    return round.area(circle_round);
}

double angled_triangle_area(AngledTriangle triangle)
{
    double len;
    double widht;
    
    ifstream fin_triangle("/Users/gordiy/cpp/area/area/triangle.txt");
    
    fin_triangle >> len;
    fin_triangle >> widht;
    
    triangle.set_len(len);
    triangle.set_width(widht);
    
    return triangle.area();
}

double trapezium_area(Trapezium trapezium)
{
    double a;
    double b;
    double high;
    
    ifstream fin_trapezium("/Users/gordiy/cpp/area/area/trapezium.txt");
    
    fin_trapezium >> a;
    fin_trapezium >> b;
    fin_trapezium >> high;
    
    trapezium.set_a(a);
    trapezium.set_b(b);
    trapezium.set_high(high);
    
    return trapezium.area();
}

int main(int argc, const char * argv[]) {
    
    Rectangle rectangle;
    cout << rectangle_area(rectangle) << endl;
    
    Round round;
    cout << round_area(round) << endl;
    
    AngledTriangle triangle;
    cout << angled_triangle_area(triangle) << endl;
    
    Trapezium trapezium;
    cout << trapezium_area(trapezium) << endl;
    
    return 0;
}
